#coding=utf-8
import cPickle

data = {
    "blog_name":'运维笔记',
    "subtitle":'大道至简，守正，出奇',
    "description":"WLBLOG",
    "keywords":"Linux,Python",
    "duoshuo":"",
    "mdpath":'md',
    "user":{'admin':'admin@WLBLOG'},
    "article":{
        1:{
            'title':'WLBLOG系统 v2.0发布',
            'arg':'WLBLOG,Python',
            'stick':'1',    #置顶排序，为0表示不置顶
            'group':'项目',
            'access':0,     #0表示所有无论是否登入都可以访问。其他表示私有，只有登入之后才可访问。
            'Date':'2016-11-28 10:15'
        },
        2:{
            'title':'这只是一个测试',
            'arg':'Hello world',
            'stick':'0',
            'group':None,
            'access':1,
            'Date':'2016-11-28 18:18'
        },
               },
    "link":{1:('运维笔记','http://wangxun.me'),2:('wenrouge','http://wangxun.me')},
    "group":['python','Linux'],
    "advert":{
        "begin":'',
        "middle":'',
        "end":''
    },

}

cPickle.dump(data,open('config.pk','wb',True))