
### 更新内容&特性：
* 不再需要数据库
* 修复了1.0的已知BUG
* 极简主题
* 改动太大，代码重写99.99%，所以和1.0完全不兼容。

###特性
* 静态部署，随时可以打包迁移。
* 动静分离，可以只部署静态内容，当然喜欢线上写博的也可以动态部署。
* 代码高亮示例：

        class session(object):
            def __init__(self,request):
                sessionid = '__sessionid__'
                self._request = request
                self._id = request.get_cookie(sessionid,'')
                if not self._id:
                    self._id = create_sessionkey()
                    request.set_cookie(sessionid,self._id,expires_days=None,expires=time.time()+900)
        
            def __getitem__(self,item):
                return session_container[self._id][item]
            
            def __setitem__(self,item,value):
                if session_container.has_key(self._id):
                    session_container[self._id][item] = value
                else:
                    session_container[self._id] = {item:value}
        
            def __delitem__(self,item):
                del(session_container[self._id])
